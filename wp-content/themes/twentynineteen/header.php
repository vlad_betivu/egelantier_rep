<!DOCTYPE html>
<html lang="en">
<head>
	<style>
	.navigator {
	  position: absolute;
	  top: 0;
	  width: 100%;
	  height: 75px;
	  display: grid;
	  grid-template-columns: 1fr 1fr 1fr;
	  background: #fff;
	}

	.navigator a {
	  text-decoration: none;
	  outline: none;
	}

	.logo_container {
	  align-self: end;
	  height: 65px;
	  padding-left: 20px;
	  padding-bottom: 5px;
	}

	.logo_container img {
	  height: 100%;
	}

	.links_container {
	  align-self: end;
	  justify-self: center;
	  display: grid;
	  grid-template-columns: 1fr 1fr;
	  padding-bottom: 5px;
	}

	.nav_link {
	  font-family: "Nobile-Regular";
	  color: #C5D2E2;
/* 	  font-size: 13px; */
/* 	  font-size: 10px; */
	  font-size: 12px;
	}

	.home_link {
	  align-self: end;
	  justify-self: end;
	  color: #2C3594;
	  margin-right: 20px;
	  margin-bottom: 5px;
/* 	  font-size: 13px; */
	  font-size: 10px;
	  font-family: "Nobile-Medium";
	}

		.current_link{
			color: #2C3594;
		}

	</style>
</head>
<body>
  <nav class="navigator">
    <div class="logo_container">
      <img src="https://ar1.ftjrn.com/wp-content/uploads/2018/12/logo.png" alt="logo" />
    </div>
    <div class="links_container">
      <a href="/lorenz-page" class="nav_link nav_lorenz">LORENZ DIET</a>
      <a href="/mach-page" class="nav_link nav_mach">MACHTELD DE ROO</a>
    </div>
<!--     <a href="/" class="home_link">HOME</a> -->
  </nav>
  <script>
	  if (window.location.href.indexOf("/lorenz-page") >= 0){
		  document.querySelector(".nav_lorenz").classList.add("current_link");
	  } else if (window.location.href.indexOf("/mach-page") >= 0){
		  document.querySelector(".nav_mach").classList.add("current_link");
	  }
  </script>
</body>
</html>
