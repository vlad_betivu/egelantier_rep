<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <title>Document</title>
  <style>
    .page_bottom {
      color: #fff;
    }

    .page_bottom a {
      outline: none !important;
      text-decoration: none !important;
      color: #fff;
    }

    .info_container {
      /*font-size: 13px;*/
/* 	  font-size: 10px; */
	  font-size: 12px;
      line-height: 15px;
      font-family: "Nobile-Bold";
      height: 135px;
      background: #2C3594;
      display: grid;
      grid-template-columns: 1fr 1fr 1fr;
      align-items: center;
      justify-items: center;
    }

    .footer_logo {
      height: 75px;
    }

    .footer_logo img {
      height: 100%;
    }


    .left_info {
      margin-left: 250px;
    }

    .info_text {
      display: block;
/*       font-size: 12px; */
/* 	  font-size: 9px; */
	  font-size: 11px;
      font-family: "Nobile-Regular";
    }

    .info_caption {
      display: block;
      margin-bottom: 10px;
/*       font-size: 13px; */
	  font-size: 10px;
      font-family: "Nobile-Bold";
      margin-top: 8px;
    }

    .right_info {
      text-align: right;
      margin-left: -250px;
    }

    .author {
      height: 35px;
      background: rgb(183, 201, 220);
/*       font-size: 12px; */
	  font-size: 9px;
      font-family: "Nobile-Regular";
      display: grid;
      align-items: center;
      justify-items: center;
    }

    .page_bottom i {
/*       font-size: 10px; */
	  font-size: 7px;
    }
	  
	  @media (max-width: 800px){
		      .right_info {
				justify-self: end;
				padding-right: 10px;
				margin-left: 0;
			  }

			  .left_info {
				padding-left: 10px;
				margin-left: 0;
			  }
	  }
  </style>
</head>

<body>
  <footer class="page_bottom">
    <div class="info_container">
      <div class="left_info">
        <span class="info_caption">Praklijk De Egelantier</span>
        <span class="info_text">Egelantierenstraat 5</span>
        <span class="info_text">8020 Oostkamp</span>
        <span class="info_text">info@praktijkdeegelantier.be</span>
      </div>
      <div class="footer_logo">
        <img src="https://ar1.ftjrn.com/wp-content/uploads/2018/12/foot.png" alt="">
      </div>
      <div class="right_info">
        <span class="info_caption">Sitemap</span>
        <a href="/" class="info_text">Homepagina</a>
        <a href="/lorenz-page" class="info_text">Lorenz Diet</a>
        <a href="/mach-page" class="info_text">Machteld De Roo</a>
      </div>
    </div>
    <div class="author">
      <div>
        <i class="far fa-copyright"></i>
        <span class="author_name"> Group Van Damme 2018</span>
      </div>
    </div>
  </footer>
</body>

</html>
