<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="style_test.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
</head>

<body>
  <?php get_header();?>
  <div class="main">
    <div class="lorenz_container">
      <div class="lorenz_list main_list_container">
        <span class="lorenz_list_item main_list_item">KINESITHERAPIE</span>
        <span class="lorenz_list_item main_list_item">MANUELE THERAPIE</span>
        <span class="lorenz_list_item main_list_item">REVALIDATIE</span>
        <span class="lorenz_list_item main_list_item">MEDISCHE OEFENTHERAPIE</span>
      </div>
      <a href="/lorenz-page" class="department_container">
        <i class="arrow_icon far fa-arrow-alt-circle-down"></i>
        <span class="department_name">LORENZ DIET</span>
      </a>
    </div>
    <div class="main_logo">
      <img src="https://ar1.ftjrn.com/wp-content/uploads/2018/12/logo.png" alt="logo" />
    </div>
    <div class="mach_container">
      <div class="mach_list main_list_container">
        <span class="mach_list_item main_list_item">WORDT VERWACHT:</span>
        <span class="mach_list_item main_list_item">&nbsp;</span>
        <span class="mach_list_item main_list_item">GESPECIALISEERDE VOETVERZORGING</span>
        <span class="mach_list_item main_list_item">MEDISCHE PEDICURE</span>
      </div>
      <a href="/mach-page" class="department_container">
        <span class="department_name">MACHTELD DE ROO</span>
        <i class="arrow_icon far fa-arrow-alt-circle-down"></i>
      </a>
    </div>
  </div>
  <?php get_footer();?>
</body>

</html>
