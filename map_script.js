function initMap() {
  var myLatLng = {
    lat: 51.174964,
    lng: 3.244039
  };
  var map = new google.maps.Map(document.querySelector('.block_map'), {
    center: myLatLng,
    zoom: 17
  });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
  });
}
